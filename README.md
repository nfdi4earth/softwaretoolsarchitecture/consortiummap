# ConsortiumMap

ConsortiumMap is an HTML file including CSS and JavaScript for creating a [Leaflet](https://leafletjs.com/) map and an interactive HTML list of the [NFDI4Earth](https://www.nfdi4earth.de/)'s participants.

They are generated automatically from the [NFDI4Earth Knowledge Hub](https://nfdi4earth.de/2facilitate/onestop4all) (KH) queried using [SPARQL](https://sparql.knowledgehub.nfdi4earth.de/#/) based on information provided in [NFDI4Earth and its members on Wikidata](https://www.wikidata.org/wiki/Q108542504) and the [Research Organisation Registry (ROR)](https://ror.org/).
In addition, a CSV file in this repository is harvested to provide information about the organisation contact persons.

## Live

<https://nfdi4earth.pages.rwth-aachen.de/onestop4all/consortiummap/>

Data source: <https://knowledgehub.nfdi4earth.de/>

## Consortium members and contact persons metadata

### Overview

The contact persons are connected with their organisations with a single spreadsheet in this repository, `nfdi4earth_contacts.csv`.
This spreadsheet is regularly harvested and information is enriched from the above mentioned sources to be inserted in the KH.
The spreadsheet must contain name and email for contact persons and either the organisation's ROR ID or (as a fallback) the organisation's Wikidata ID.
If available, the ORCID of contact persons is also stored.

### Adding a new member to the consortium

1. Reach out to the current main contact person of the new member to confirm that...
    1. they are the contact person to add to the website
    1. they provide logo persmissions (see below); you update the information in [the logo spreadsheet](https://sharepoint.tu-dresden.de/projects/nfdi4earth/Dokumenteintern/%C3%96ffentlichkeitsarbeit/Homepage/NFDI4Earth_Logos_Consortium_20230808.xlsx?d=w9e63a40b9c114ce3bb1084072170a613)
    1. they confirm their ORCID
    1. they confirm the organisation ROR
1. Wait for confirmation (and ideally for usage rights confirmation, though this may also arrive afterwwards)
1. Add the new member to the Wikidata record of NFDI4Earth, namely <https://www.wikidata.org/wiki/Q108542504>, carefully adding the correct qualifier for the different roles (see existing entries, there is more than one "participant" qualifier)
1. Add the contact person and organissation identifiers to the contact person table in `nfdi4earth_contacts.csv` and push the update _to the main repository on GitLab, not just your fork_
1. Ping the Knowledge Hub team (Jonas G.) to trigger the updating process until the [scheduled harvesting](https://git.rwth-aachen.de/groups/nfdi4earth/-/epics/2) works
1. After the map is updated, check if the logo is correctly rendered using Wikidata; if not, add a fallback logo to the NFDI4Earth website and provide the required URL in the data object in `map/n4e-consortium-map.js`

### Updating the information

The contact persons for all consortium members, including the metadata about consortium members, are checked and confirmed **annually** _after the summer_ (e.g., in late September).
The last annual update was initiated in August 2023 by DN.

1. Check if the list of consortia shown on the members page is still current (check with JS and UA for new contracts etc.)
1. Prepare an email text to all contact persons, which asks for a confirmation of the correctness of their own and the consortium metadata (see columns in the contacts spreedsheet); check with the NFDI4Earth coordination office if any other information shall be requested in the same mailing, e.g., updated persmissions for using logos, etc.
1. Create an email template from the text to send the messages to all contact persons in the spreadsheet - see instructions in [Email](#email) below.
1. Handle the responses
    1. If a new contact person is presented, continue confirmation process with new person
    1. Update the information in the contacts spreadsheet in this GitHub repository
    1. Update the `confirmation` column in the contacts spreadsheet to reflect when and how the confirmation was provided
    1. Save received digital documents in the corresponding directory in the internal SharePoint in `Öffentlichkeitsarbeit/Nutzungsrechte Logos`, using the ROR ID (alternatively Wikidata ID) in the file name.
    1. Sort emails in the coordination office's inbox into a fitting sub-folder fo `Contacts` (get login form CH or DN)
    1. (If applicable) Update the rights for using logos for the homepage ("Nutzungsrechte") are documented in [a spreadsheet](https://sharepoint.tu-dresden.de/projects/nfdi4earth/Dokumenteintern/%C3%96ffentlichkeitsarbeit/Homepage/NFDI4Earth_Logos_Consortium_20230808.xlsx?d=w9e63a40b9c114ce3bb1084072170a613) in internal SharePoint in `Öffentlichkeitsarbeit/Homepage`
1. Revisit the `To-Do` folder in the coordination office inbox
1. After all responses have been processed, double check that the regular harvesting updated the data in the Knowledge Hub

### Email template for annual information update

To get in touch with the NFDI4Earth contact persons, e.g., for asking to valiate the current information, the email client Mozilla Thunderbird (Version 115.1.0) and its add-on [Mail Merge](https://addons.thunderbird.net/de/thunderbird/addon/mail-merge/) are used.
The Mail Merge add-on page has step by step instructions for using it with a CSV file.
If you want to use the plugin with a functional email address, you may need to create a [Thunderbird Profile](https://support.mozilla.org/en-US/kb/using-multiple-profiles) and start it with `thunderbird - ProfileManager`.

The following template can be completed automatically with the information from `nfdi4earth_contacts.csv`.

```txt
Dear {{name}},

the NFDI4Earth team is currently preparing the annual update for the list of consortium members and their contact persons and confirming permissions to use logos. Please reply by *September 8th* to both inquiries below.


*Contact information*

Please confirm that you are still the contact for NFDI4Earth by briefly replying to this message, or forward this message to a more suitable contact person in your organisation so we can update the data. Note that the information will be publicly displayed on the NFDI4Earth website [1] and stored in the NFDI4Earth Knowledge Hub.

Please verify the following metadata about you and your organisation:

Name: {{name}}

Email: {{email}}

ORCID: {{orcid}} [2]


LINK TO ONESTOP

ROR ID (may be missing): {{org_ror}} [3]
Wikidata ID (only if there is no ROR ID): {{wikidata_id}} [4]

The ROR ID and Wikidata IDs are used to retrieve metadata about your organisation, such as the name or logo, from the public ROR database and Wikidata. The ORCID is used to retrieve publicly available information about you. These are the normative sources for the metadata and in case we discovered missing data, errors or inconsistencies, we have reached out to these organisations to have the information corrected.
We kindly invite you to take the opportunity and check if the public information on your ORCID profile [2] is still up to date, because we will use the affiliation and email from ORCID if it is available.

Please provide the following new information:

Institution name and link: *One or multiple links to the relevant working group, team, or institution.*

We would like to add these links to the consortium map to make it easier for interested parties to contact the right people and understand who actually is involved, as currently the formal/legal member is listed by name. Thanks!

Finally, please take a moment to confirm that you are on the relevant mailing lists to stay up to date:

- the NFDI4Earth plenum is a public mailing list for the broad ESS community with about one message per month, including the NFDI4Earth Newsletter: https://mailman.zih.tu-dresden.de/groups/listinfo/nfdi4earth-plenum
- the NFDI4Earth participants ... : https://mailman.zih.tu-dresden.de/groups/listinfo/nfdi4earth-participants


ADVERTISE NEWSLETTER
ADVERTISE COMMITMENT?



*Logo usage and currentness*

Please check if your logo used on [1] is still current.

We require explicit written consent for adding your logo to the NFDI4Earth website and other uses in the context of NFDI4Earth, for example for presentations, unless your organisation's logo is published under an open license already. If the material is openly licensed, please point us to the publicly available information about the license - thanks. The letter should include a sentence along the following lines:

"X gewährt der Technischen Universität Dresden als Sprechereinrichtung der NFDI4Earth einfache Nutzungsrechte für die zeitlich unbegrenzte weltweite nicht exklusive und uneingeschränkte Verwendung unseres Logos im Zusammenhang mit dem Projekt NFDI4Earth, insbesondere für Pressearbeit, Online-Medien, soziale Netzwerke, und zur Darstellung des Projektkonsortiums."

Please send a signed document to

Technische Universität Dresden
Fakultät Umweltwissenschaften
Professur für Geoinformatik
z.H. Fr. Henzen

Helmholtzstraße 10
01069 Dresden


Don't hesitate to get in touch with us if you want to update the information in the future outside of the annual update, or if you have any questions - thank you in advance!

Kind regards,
NFDI4Earth coordination team

[1] https://nfdi4earth.de/about-us/consortium
[2] https://orcid.org/{{orcid}}
[3] https://ror.org/{{org_ror}}
[4] https://www.wikidata.org/wiki/{{wikidata_id}}
```

## Libraries

- Leaflet `1.9.4` from <https://leafletjs-cdn.s3.amazonaws.com/content/leaflet/v1.9.4/leaflet.zip>
- jQuery `3.7.0` from <https://code.jquery.com/jquery-3.7.0.min.js> and <https://code.jquery.com/jquery-3.7.0.min.map>
- MapLibre GL JS `2.4.0` from <https://github.com/maplibre/maplibre-gl-js/releases/tag/v2.4.0>
- MapLibre GL Leaflet `0.0.17` from <https://unpkg.com/@maplibre/maplibre-gl-leaflet@0.0.17/leaflet-maplibre-gl.js>

Not required files were removed from the download packages.

## Development

### Run locally

To run the application locally, you can open the file `index.html` in your browser.
Note that you might have to use a browser plugin to disable web security and enable any CORS requests.

Alternatively, run the following command to start a webserver in a container:

```bash
docker run --rm -p 80:80 -v $(pwd):/usr/share/nginx/html/ nginx
```

Then visit <http://localhost/index.html>.

### Tests

The front end testing tool for web applications [Cypress](https://docs.cypress.io/guides/overview/why-cypress) (tested with version 12.17.4) is used for testing.
Its website includes step by step instructions on how to set it up.

The folder `cypress` has two files:

- `consortium-list.cy.js`: Tests for the list, e.g., all consortium members are listed
- `consortium-map.cy.js`: Tests for the map, e.g., the number of markers is correct

You can run the tests locally or check results in the GitLab CI (see also `.gitlab-ci.yml` for more commands):

```bash
# rm -r node_modules/
# npm install
npx cypress run --browser chrome
```

## Deployment

The integration in the current Joomla-based NFDI4Earth website is a bit tricky.
Here are the most important building blocks:

- The Nicepage page for the consortium has a "Block/Position" with the position name "map" ("Position und Widgetbereich")
- A ["custom module" for Joomla](https://nfdi4earth.de/administrator/index.php?option=com_modules&view=modules&client_id=0) is the way to simply enter the HTML code (most content from `index.html`)
  - The module is configured to insert itself in at the position named "map"
  - The CSS code is loaded via JS because we cannot access the HTML head in the module
- The JavaScript and CSS files are saved on the server directly via SCP, and are also accessible at <https://nfdi4earth.de/map/>; updates must be done manually; this is also the reason for the local directory layout

## License

Licensed under the [Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0) except the contents of the folder "map/lib", see individual files for more information.
