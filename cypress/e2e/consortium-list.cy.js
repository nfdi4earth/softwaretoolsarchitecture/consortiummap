describe('the list of consortium members', () => {

  var members = 64;

  beforeEach(() => {
    cy.visit('index.html');
    cy.wait(3000);
  });

  it('should list all ' + members + ' members', () => {
    cy.get('#membersGrid .member').should('have.length', members);
  });

  it('should be sorted alphabetically', () => {
    cy.get('#membersGrid .member').first().should('contain', 'Alfred Wegener Institute');
    cy.get('#membersGrid .member').last().should('contain', 'Water Science Alliance');
  });

  it('must include a name of contact for every member', () => {
    cy.get('#membersGrid .member').get('.contactNameCard').should('have.length', members);
  });

  it('must include the email of contact for every member as a clickable link', () => {
    cy.get('#membersGrid .member').get('.emailCard').should('have.length', members);

    // Check if there is a HREF attribute with mailto
    cy.get('#membersGrid .member').get('.emailCard').each(($row) => {
      var email = $row.get(0).outerHTML;
      cy.expect(email).to.contain('<a href="mailto:')
    });
  });

  it('should open the right popup', () => {
    cy.get('#membersGrid .member').get('.nameCard').each(($row) => {
      var name = $row.get(0).innerText; // Get name that is written on the card
      cy.get($row).click();
      cy.get('.namePopup').last().should('contain', name);
    });
  });

  it('should show an individual logo for each member', () => { // Count the number of the default N4E logo
    cy.get('#membersGrid .member').get('.logoDivGrid').get('.logoDiv > img').get('[src="map/nologo.png"]').should('have.length', 0);
  });

});
