/*
Copyright 2023 TU Dresden, NFDI4Earth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

var map;

// Executed once the web page is completely loaded
$(document).ready(function () {

    // Show main div if JavaScript enabled and available
    document.getElementById("divMain").style.display = "block";

    map = L.map("divMap").setView([51.16, 10.45], 6); // Geographical coordinates and zoom level of the map

    /*
    // Tile layer of the map using https://basemap.de/web-vektor/
    L.maplibreGL({
        style: 'https://sgx.geodatenzentrum.de/gdz_basemapde_vektor/styles/bm_web_col.json',
        attribution: 'Map data: &copy <a href="https://www.wikidata.org/wiki/Wikidata:Licensing">Wikidata</a>  &amp; <a href="https://ror.org/terms-of-use/">ROR</a> | Background tiles: &copy; <a href="https://basemap.de/web-vektor/">basemap.de</a> / BKG' // https://sgx.geodatenzentrum.de/web_public/gdz/lizenz/deu/basemapde_web_dienste_lizenz.pdf
    }).addTo(map);
    */

    // Tile layer of the map using https://gdz.bkg.bund.de/index.php/default/webdienste/topplus-produkte/wmts-topplusopen-wmts-topplus-open.html
    L.tileLayer("https://sgx.geodatenzentrum.de/wmts_topplus_open/tile/1.0.0/web_light/default/WEBMERCATOR/{z}/{y}/{x}.png", {
        format: "image/png",
        attribution: 'Map data: &copy <a href="https://www.wikidata.org/wiki/Wikidata:Licensing">Wikidata</a>  &amp; <a href="https://ror.org/terms-of-use/">ROR</a> | Background tiles: &copy  Bundesamt  f&uumlr  Kartographie  und  Geod&aumlsie (BKG),  <a  href= "https://sgx.geodatenzentrum.de/web_public/gdz/datenquellen/Datenquellen_TopPlusOpen.html">data sources</a>'
    }).addTo(map);

    // Reset map button
    $("#resetMapButton").on("click", function () {
        map.closePopup();
        map.setView([51.16, 10.45], 6);
        $(window).scrollTop($('#divMap').offset().top);
    });

    // Markers
    var allMarkers = []; // Array for the markers
    var markers = L.markerClusterGroup();

    // Define functions
    function onEachMember(participants, number, success, error) {
        currentParticipant = participants["@graph"][number];
        var identifier = currentParticipant["@id"].split("/").pop(); // Unique identifier for each participant

        // Homepage
        var homepage = undefined; // Fallback

        if (currentParticipant["homepage"]) {
            url = undefined;
            if (Array.isArray(currentParticipant["homepage"]) && currentParticipant["homepage"].length > 1) { // If there is more than one homepage, choose the first one
                url = currentParticipant["homepage"][0];
            } else {
                url = currentParticipant["homepage"];
            }
            
            hostname = new URL(url).hostname;
            var homepage = "<a href='" + url + "' target='_blank'>" + hostname + "</a>";
        };

        // Location
        var lat = []; // Fallback
        var lng = []; // Fallback

        if (currentParticipant["hasGeometry"]) {
            var geometries = [];

            if($.isArray(currentParticipant["hasGeometry"])) {
                console.debug("%s has more than one geometry, namely %s", currentParticipant["@id"], currentParticipant["hasGeometry"].length);

                $.each(currentParticipant["hasGeometry"], function (index, geometry) {
                    geometries.push(currentParticipant["hasGeometry"]);
                });
            } else {
                geometries.push(currentParticipant["hasGeometry"]);
            }

            $.each(geometries, function (index, geometry) {
                var geometryType = geometry["@type"];

                if (geometryType == "http://www.opengis.net/ont/sf#Point") {
                    var location = geometry["asWKT"];

                    // Save lng and lat in arrays
                    if (location["@value"].split(" ").length == 2) {
                        lng.push(location["@value"].split(" ")[0].split("(")[1]);
                        lat.push(location["@value"].split(" ")[1].split(")")[0]);
                    }

                    else {
                        lng.push(location["@value"].split(" ")[1].split("(")[1]);
                        lat.push(location["@value"].split(" ")[2].split(")")[0]);
                    };
                }
                else {
                    console.debug("Unsupported location type '%s'. Only sf:Point is supported.", geometryType);
                };
            });
        }

        // Logo
        var logo = "map/nologo.png"; // Fallback

        // If there is no logo from Wikidata, fall back to local image files
        var sourceSystemID = currentParticipant["sourceSystemID"]; // ROR ID or Wikidata ID

        if (currentParticipant["hasLogo"]) {
            // Logo is available in the KH
            var logo = currentParticipant["hasLogo"]["@value"];
        }
        else {
            var memberWithoutLogo = missingLogos.find(element => element["sourceSystemID"] == sourceSystemID);

            if (memberWithoutLogo) {
                logo = memberWithoutLogo["logoURL"];
            }
            else {
                console.debug("No logo available for sourceSystemID %s | %s", sourceSystemID, JSON.stringify(currentParticipant["foaf:name"]));
            };
        };

        // Name
        var name = currentParticipant["@id"]; // Use identifier as fallback name

        if (currentParticipant["name"]) {
            if($.isArray(currentParticipant["name"])) {
                var nameEN = currentParticipant["name"].find(element => element["@language"] == "en");
                var nameDE = currentParticipant["name"].find(element => element["@language"] == "de");

                // Search for English name
                if (nameEN) {
                    var name = nameEN["@value"];
                }
                // Take name without language tag if english name doesn't exist
                else if (currentParticipant["name"].find(element => typeof element.length !== "undefined")) {
                    var name = currentParticipant["name"].find(element => typeof element.length !== "undefined");
                }
                // Eventually  also take the German name
                else if (nameDE) {
                    var name = nameDE["@value"];
                };
            } else {
                // just one name, maybe because ROR has no translations
                name = currentParticipant["name"];
            };
        }

        // Contact person
        var contactPerson = undefined;
        var email = undefined;

        var urlContact = currentParticipant["hasNFDI4EarthContactPerson"];

        // Role
        var role = undefined;
        if (currentParticipant["role"]) {
            if(currentParticipant["role"] == "http://www.wikidata.org/entity/Q54875338") {
                role = "Applicant";
            } else if(currentParticipant["role"] == "http://www.wikidata.org/entity/Q105906738") {
                role = "Co-applicant";
            } else if(currentParticipant["role"] == "http://www.wikidata.org/entity/Q105906860") {
                role = "Participant";
            } else if(currentParticipant["role"] == "http://www.wikidata.org/entity/Q83878004") {
                role = "Network";
            } else {
                role = "&nbsp;"
            }
        } else {
            role = "&nbsp;"
        }


        if (name && homepage && logo && urlContact && urlContact.startsWith("http") && role) {

            ajax = $.ajax({

                dataType: "JSON",
                url: urlContact,
                success: function (contactInformation) {

                    contactPerson = contactInformation["name"][0];
                    if (contactPerson) {
                        email = contactInformation["email"][0];
                    }

                    createPopupAndList(name, homepage, logo, lat, lng, identifier, contactPerson, email, role, success, error);

                    map.addLayer(markers);
                },
                error: function () {
                    console.error("Getting contact information failed for " + name);
                    console.debug(name, homepage, logo, lat, lng, identifier, contactPerson, email, role, success, error);
                    error({
                        error: "Getting contact information failed for " + name
                    });
                }
            });
        }
        else {
            console.error("Incomplete record for id %s | name: %s, homepage: %s, logo: %s, lat: %s, lon: %s, contactPerson: %s, email: %s", currentParticipant["@id"], name, homepage, logo, lat, lng, contactPerson, email);
            success({
                error: "Getting contact full information for ID " + currentParticipant["@id"] + " with name: " + name
            });
        };

        return(role);
    };

    // Function to create popup und list element
    function createPopupAndList(name, homepage, logo, lat, lng, identifier, contactPerson, email, role, success, error) {

        if(email.startsWith("http")) {
            emailPopup = emailCard = "<a href='" + email + "' title='Send email to contact person'>Send email</a>";
        } else {
            emailPopup = "<a href='mailto:" + email + "' title='Send email to contact person'>Send email</a>";
            emailCard = "<a href='mailto:" + email + "' title='Send email to contact person'>" + email + "</a>";
        }
        
        popupContent = "";
        cardContent = "";

        // HTML templates as functions
        function templatePopup(logo, name, homepage, contactPerson, emailPopup, role) {

            if (contactPerson != 0 && email != 0) { // If there is a contact person
                // Use divider for logo to make sure that the popup is centered over the marker
                return `
                <div class='logoDiv'><img src=${logo} class='logo'></img></div><br/>
                <span class='namePopup'>${name}</span><br/>
                <span class='role' style='color: ${colourForRole(role)};'>(${role})</span><br/><br/>
                <span class='homepagePopup'>${homepage}</span><br/><br/>
                <span class='contactNamePopup'>${contactPerson}</span><br/>
                <span class='emailPopup'>${emailPopup}</span>
                `
            }

            else {
                return `
                <div class='logoDiv'><img src=${logo} class='logo'></img></div><br/>
                <span class='namePopup'>${name}</span><br/>
                <span class='role'>(${role})</span><br/><br/>
                <span class='homepagePopup'>${homepage}</span>
                `
            };
        };

        function templateCard(identifier, logo, name, homepage, contactPerson, emailCard, role) {

            if (contactPerson != 0 && email != 0) { // If there is a contact person
                return `
                <div class='member' id=${identifier}>
                    <div class='logoDivGrid'><div class='logoDiv'><img src=${logo} class='logo'></img></div></div>
                        <hr class='hrGrid'></hr>
                        <div class='text'>
                            <b><span class='nameCard'>${name}</span></b>
                            <br><span class='role' style='color: ${colourForRole(role)};'>${role}</span></br>
                            <br>
                            <br>${homepage}</br>
                            <br></br>
                            <span class='contactNameCard'>${contactPerson}</span>
                            <br><span class='emailCard'>${emailCard}</span></br>
                        </div>
                    </div>
                </div>
                `
            }

            else {
                return `
                <div class='member' id=${identifier}>
                    <div class='logoDivGrid'><div class='logoDiv'><img src=${logo} class='logo'></img></div></div>
                        <hr class='hrGrid'></hr>
                        <div class='text'>
                            <b><span class ='nameCard'>${name}</span></b>
                            <br></br>
                            <br>${homepage}</br>
                        </div>
                    </div>
                </div>
                `
            };
        };

        popupContent = templatePopup(logo, name, homepage, contactPerson, emailPopup, role);
        cardContent = templateCard(identifier, logo, name, homepage, contactPerson, emailCard, role);

        $("#membersGrid").append(cardContent);

        if (lat != [] && lng != []) {
            // Markers, each one has a unique ID (Everything after the last "/" of "@id"), and popups

            for (let i = 0; i < lat.length; i++) {
                var marker = L.marker([lat[i], lng[i]], {
                    id: identifier,
                    icon: createPersonalIcon(role),
                    role: role
                }).addTo(markers).bindPopup(popupContent);

                marker.on('click', function () {
                    var id = marker.options.id; // ID of clicked marker
                    var selected = [];

                    // Reset the colour of all the markers and get all markers with the same ID for members with multiple locations
                    $.each(allMarkers, (index, marker) => {
                        marker.setIcon(createPersonalIcon(marker.options.role));

                        if (marker.options.id === id) {
                            selected.push(marker);
                        };
                    });

                    for (let i = 0; i < selected.length; i++) {
                        selected[i].setIcon(highlightIcon); // Change colour to contrasting one for all the selected markers
                        selected[i].setZIndexOffset(1000); // Move selected markers to the front to make them visible
                    };
                });

                marker.getPopup().on('popupclose', function () {
                    var id = marker.options.id;
                    var selected = [];

                    // Reset the colour and the z-index of all the markers with the same ID
                    $.each(allMarkers, (index, marker) => {
                        if (marker.options.id === id) {
                            selected.push(marker);
                        };
                    });

                    for (let i = 0; i < selected.length; i++) {
                        selected[i].setIcon(createPersonalIcon(marker.options.role)); // Reset if popup is closed
                        selected[i].setZIndexOffset(0);

                        if (!selected[i].__parent) lastMarker.__parent.unspiderfy(); // Remove spiders if there is a parent
        
                    };
                });

                // Add to array
                allMarkers.push(marker);
            };

        };

        success({
            id: identifier,
            marker: marker
        });
    };

    // Function to open popup automatically by comparing title of the marker and the ID of clicked element
    function markerOpenPopup(id) {        
        var selected = [];

        // Get all markers with the same ID for members with multiple locations
        $.each(allMarkers, (index, marker) => {
            if (marker && marker.options.id === id) {
                selected.push(marker);
            };
        });

        for (let i = 0; i < selected.length; i++) {
            selected[i].setIcon(highlightIcon); // Change colour to contrasting colour if selected
            selected[i].setZIndexOffset(1000); // Move selected markers to the front to make them visible
        };

        lastMarker = selected[selected.length - 1];

        if(!lastMarker) {
            console.error('No marker found for id ', id);
            return false;
        }

        if (!lastMarker._icon && lastMarker.__parent) lastMarker.__parent.spiderfy(); // https://github.com/Leaflet/Leaflet.markercluster/issues/72#issuecomment-112773750
        lastMarker.openPopup(); // Open popup of the last marker
        $(window).scrollTop($('#divMap').offset().top);
        return true;
    };

    const n4eBlue = "#05668D"; // Marker colours: NFDI4Earth blue with white border
    const n4eLightBlue = "#C0D6DF"; // Marker colours: NFDI4Earth light blue with white border
    const n4eDarkBlue = "#003F5F"; // Marker colours: NFDI4Earth dark blue with white border
    const n4eYellow = "#F1C129";

    const highlightIcon = L.divIcon({
        className: "n4eIcon",
        iconAnchor: [10, 25],
        popupAnchor: [1.5, -20],
        html: `<span style='background-color: ${n4eYellow}; display: block; width: 20px; height: 20px; border-radius: 20px 20px 0; transform: rotate(45deg); border: 1px solid #FFFFFF;'></span>`
    });

    function colourForRole(role) {
        colour = n4eBlue;

        switch(role) {
            case "Co-applicant":
                colour = n4eBlue;
            case "Participant":
                colour = n4eBlue;
                break;
            case "Network":
                colour = n4eLightBlue;
                break;
            case "Applicant":
                colour = n4eDarkBlue;
                break;
            default:
                console.error("Unknown role, cannot select colour: ", role);
        }

        return(colour);
    }

    function createPersonalIcon(role) {

        colour = colourForRole(role);

        markerHtmlStyle = `
        background-color: ${colour};
        display: block;
        width: 20px;
        height: 20px;
        border-radius: 20px 20px 0;
        transform: rotate(45deg);
        border: 1px solid #FFFFFF
        `

        return L.divIcon({
            className: "n4eIcon",
            iconAnchor: [10, 25],
            popupAnchor: [1.5, -20],
            html: `<span style='${markerHtmlStyle}'></span>`
        });

    };

    // Get logos that are missing on Wikidata or that are not scaled correctly from https://nfdi4earth.de/about-us/consortium (files managed in Joomla)
    // Use KH sourceSystemID (ROR ID or Wikidata ID)
    var missingLogos = [

        {
            // Alfred Wegener Institute for Polar and Marine Research
            "sourceSystemID": "032e6b942",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/AWI_Logo_2017.png"
        },
        {
            // Bavarian State Archives
            "sourceSystemID": "01hq2jk95",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/sab.png"
        },
        {
            // Bochum University of Applied Sciences
            "sourceSystemID": "04x02q560",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/BO-Logo_m_Wortmarke_L.png"
        },
        {
            // Deutscher Wetterdienst
            "sourceSystemID": "02nrqs528",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/DWD-Logo_2013.png"
        },
        {
            // Free University of Berlin
            "sourceSystemID": "046ak2485",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/fuberlin_logo.png"
        },
        {
            // GEOMAR Helmholtz Centre for Ocean Research Kiel
            "sourceSystemID": "02h2x0161",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/geomar.jpg"
        },
        {
            // Geo.X
            "sourceSystemID": "Q115265089",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/GeoX_ausgeschnitten.png"
        },
        {
            // Geoverbund ABC/J
            "sourceSystemID": "Q121462511",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/abcj_geoverbund-logo.png"
        },
        {
            // German Climate Computing Centre
            "sourceSystemID": "03ztgj037",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/dkrz_logo.png"
        },
        {
            // German Marine Research Alliance
            "sourceSystemID": "03rgygs91",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/dam.jpg"
        },
        {
            // Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
            "sourceSystemID": "04z8jg394",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/GFZ-Wortbildmarke-DE-Helmholtzdunkelblau_RGB.png"
        },
        {
            // Helmholtz-Zentrum Geesthacht Centre for Materials and Coastal Research
            "sourceSystemID": "03qjp1d79",
            logoURL: "https://nfdi4earth.de/images/nicepage-images/hereon_Logo_standard_03_rgb.png"
        },
        {
            // Jülich Research Centre
            "sourceSystemID": "02nv7yv05",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/FZj%C3%BClich_Logo.jpg"
        },
        {
            // Leibniz Centre for Agricultural Landscape Research
            "sourceSystemID": "01ygyzs83",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/zalf.png"
        },
        {
            // Leibniz Institute for Applied Geophysics
            "sourceSystemID": "05txczf44",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/liag.png"
        },
        {
            // Leibniz Institute for Tropospheric Research
            "sourceSystemID": "03a5xsc56",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/tropos.jpg"
        },
        {
            // Leibniz Institute of Ecological Urban and Regional Development
            "sourceSystemID": "02t26g637",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/i%C3%B6r.gif"
        },
        {
            // Leibniz Institute of Freshwater Ecology and Inland Fisheries
            "sourceSystemID": "01nftxb06",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/IGB_farbe_pos.png"
        },
        {
            // Leipzig University
            "sourceSystemID": "03s7gtk40",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/Universit%C3%A4t_Leipzig_logo.png"
        },
        {
            // Max Planck Institute for Biogeochemistry
            "sourceSystemID": "051yxp643",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/BGC_LogoText_en_EPS.png"
        },
        {
            // Museum für Naturkunde - Leibniz Institute for Evolution and Biodiversity Science
            "sourceSystemID": "052d1a351",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/mfn.jpg"
        },
        {
            // Potsdam Institute for Climate Impact Research
            "sourceSystemID": "03e8s1d88",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/pik.png"
        },
        {
            // Research Institute for Sustainability
            "sourceSystemID": "01vvnmw35",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/images/RIFSPotsdam.png"
        },
        {
            // Senckenberg Nature Research Society
            "sourceSystemID": "00xmqmx64",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/Senckenberg-Logo.jpg"
        },
        {
            // Specialised Information Service for Geosciences
            "sourceSystemID": "Q47491582",
            logoURL: "https://nfdi4earth.de/images/nicepage-images/FID_GEO_Logo_rgb_randlos_max.jpg"
        },
        {
            // Technical University of Darmstadt
            "sourceSystemID": "05n911h24",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/tu_darmstadt.png"
        },
        {
            // UNU-FLORES
            "sourceSystemID": "03xp99m49",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/unu-flores.jpg"
        },
        {
            // University of Cologne
            "sourceSystemID": "00rcxh774",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/unik%C3%B6ln.png"
        },
        {
            // University of Kiel
            "sourceSystemID": "04v76ef78",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/unikiel.png"
        },
        {
            // University of Tübingen
            "sourceSystemID": "03a1kwz48",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/unit%C3%BCbingen.png"
        },
        {
            // Water Science Alliance e.V
            "sourceSystemID": "Q112117161",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/wsaev.png"
        },
        {
            // University of Freiburg
            "sourceSystemID": "0245cg223",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/Logo-Uni-Freiburg-1536x1086.png"
        },
        {
            // IOW
            "sourceSystemID": "03xh9nq73",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/logo_iow_deutsch_jpg.jpg"
        },
        {
            // DBFZ
            "sourceSystemID": "008qpg558",
            logoURL: "https://nfdi4earth.de/images/nfdi4earth/logos/logo_DFBZ_rgb.jpg"
        },
    ];

    // Query data from KH
    var query = [
        "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>",
        "PREFIX foaf: <http://xmlns.com/foaf/0.1/>",
        "PREFIX geo: <http://www.opengis.net/ont/geosparql#>",
        "PREFIX m4i: <http://w3id.org/nfdi4ing/metadata4ing#>",
        "PREFIX n4e: <http://nfdi4earth.de/ontology/>",
        "PREFIX sf: <http://www.opengis.net/ont/sf#>",
        "PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>",
        "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>",
        "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>",
        "PREFIX schema: <http://schema.org/>",
        "PREFIX org: <http://www.w3.org/ns/org#>",
        "CONSTRUCT {",
        "?organizationIRI rdfs:type foaf:Organization .",
        "?organizationIRI foaf:name ?institutionLabel .",
        "?organizationIRI foaf:homepage ?officialWebsite .",
        "?organizationIRI org:role ?roleWikidataIRI .",
        "?roleWikidataIRI rdfs:label ?role_label_en .",
        "?organizationIRI n4e:sourceSystemID ?sourceSystemID .",
        "?organizationIRI m4i:hasRorId ?rorID .",
        "?organizationIRI vcard:country-name ?countryLabel .",
        "?organizationIRI vcard:locality ?headQuartersLocationLabel .",
        "?organizationIRI vcard:hasLocality ?headQuartersLocationGeonamesID .",
        "?organizationIRI vcard:hasLogo ?logoURL .",
        "?organizationIRI geo:hasGeometry ?geom .",
        "?organizationIRI n4e:hasNFDI4EarthContactPerson ?contactPerson .",
        "?geom rdf:type sf:Point .",
        "?geom geo:asWKT ?coordinateLocation .",
        "}",
        "WHERE {",
        "?nfdi4earthProject schema:url \"https://nfdi4earth.de/\"^^xsd:anyURI .",
        "?organizationIRI org:hasMembership ?membership .",
        "?membership org:organization ?nfdi4earthProject .",
        "?membership org:role ?roleWikidataIRI .",
        "SERVICE <https://query.wikidata.org/sparql> {",
        "?roleWikidataIRI rdfs:label ?role_label_en",
        "FILTER(LANG(?role_label_en) = 'en') .",
        "}",
        "?organizationIRI schema:name ?institutionLabel .",
        "OPTIONAL {",
        "?organizationIRI foaf:homepage ?officialWebsite .",
        "}",
        "OPTIONAL {",
        "?organizationIRI n4e:sourceSystemID ?sourceSystemID .",
        "}",
        "OPTIONAL {",
        "?organizationIRI m4i:hasRorId ?rorID .",
        "}",
        "OPTIONAL {",
        "?organizationIRI vcard:country-name ?countryLabel .}",
        "OPTIONAL {",
        "?organizationIRI vcard:locality ?headQuartersLocationLabel .",
        "}",
        "OPTIONAL {",
        "?organizationIRI vcard:hasLocality ?headQuartersLocationGeonamesID .",
        "}",
        "OPTIONAL {",
        "?organizationIRI vcard:hasLogo ?logoURL .",
        "}",
        "OPTIONAL {",
        "?organizationIRI n4e:hasNFDI4EarthContactPerson ?contactPerson .",
        "}",
        "OPTIONAL {",
        "?organizationIRI geo:hasGeometry ?geom .",
        "?geom geo:asWKT ?coordinateLocation .",
        "}",
        "}"
    ].join(" ");

    var queryLRZ = [
        "PREFIX foaf: <http://xmlns.com/foaf/0.1/>",
        "PREFIX geo: <http://www.opengis.net/ont/geosparql#>",
        "PREFIX m4i: <http://w3id.org/nfdi4ing/metadata4ing#>",
        "PREFIX n4e: <http://nfdi4earth.de/ontology/>",
        "PREFIX sf: <http://www.opengis.net/ont/sf#>",
        "PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>",
        "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>",
        "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>",
        "PREFIX schema: <http://schema.org/>",
        "PREFIX org: <http://www.w3.org/ns/org#>",

        "CONSTRUCT {",
        "?organizationIRI rdfs:type foaf:Organization .",
        "?organizationIRI foaf:name ?institutionLabel .",
        "?organizationIRI foaf:homepage ?officialWebsite .",
        "?organizationIRI n4e:sourceSystemID ?sourceSystemID .",
        "?organizationIRI m4i:hasRorId ?rorID .",
        "?organizationIRI vcard:country-name ?countryLabel .",
        "?organizationIRI vcard:locality ?headQuartersLocationLabel .",
        "?organizationIRI vcard:hasLocality ?headQuartersLocationGeonamesID .",
        "?organizationIRI vcard:hasLogo ?logoURL .",
        "?organizationIRI geo:hasGeometry ?geom .",
        "?organizationIRI n4e:hasNFDI4EarthContactPerson ?contactPerson .",
        "?geom rdf:type sf:Point .",
        "?geom geo:asWKT ?coordinateLocation .",
        "}",
        "WHERE {",
        "?parentOrganizationIRI m4i:hasRorId '001rdaz60' .",
        "?organizationIRI org:subOrganizationOf ?parentOrganizationIRI .",
        "?organizationIRI schema:name ?institutionLabel .",
        "?organizationIRI foaf:homepage ?officialWebsite .",
        "OPTIONAL {",
        "?organizationIRI n4e:sourceSystemID ?sourceSystemID .",
        "}",
        "OPTIONAL {",
        "?organizationIRI m4i:hasRorId ?rorID .",
        "}",
        "OPTIONAL {",
        "?organizationIRI vcard:country-name ?countryLabel .}",
        "OPTIONAL {",
        "?organizationIRI vcard:locality ?headQuartersLocationLabel .",
        "}",
        "OPTIONAL {",
        "?organizationIRI vcard:hasLocality ?headQuartersLocationGeonamesID .",
        "}",
        "OPTIONAL {",
        "?organizationIRI vcard:hasLogo ?logoURL .",
        "}",
        "OPTIONAL {",
        "?organizationIRI n4e:hasNFDI4EarthContactPerson ?contactPerson .",
        "}",
        "OPTIONAL {",
        "?organizationIRI geo:hasGeometry ?geom .",
        "?geom geo:asWKT ?coordinateLocation .",
        "}",
        "}"
    ].join(" ");

    var url = "https://sparql.knowledgehub.nfdi4earth.de";

    $("#loadingIndicator").show();

    $.when(
        $.ajax({
            method: "POST",
            dataType: "JSON",
            url: url,
            headers: { "accept": "application/ld+json" }, // Only accept JSON-LD as a response
            data: { query: query }
        }),
        $.ajax({
            method: "POST",
            dataType: "JSON",
            url: url,
            headers: { "accept": "application/ld+json" }, // Only accept JSON-LD as a response
            data: { query: queryLRZ }
        }),
    ).done((response, response_lrz) => {
        graph = response[0];
        
        // manually transfer role from BADW Bavarian Academy of Sciences and Humanities to the subsidiary LRZ
        badw = graph["@graph"].filter(element => element["sourceSystemID"] === "001rdaz60")[0];
        response_lrz[0]["@graph"][0]["role"] = badw["role"];
        
        // remove selected parts of the graph
        graph["@graph"] = graph["@graph"]
            .filter(element => [
                "001rdaz60", // skip BADW participant from graph and add LRZ instead
                "01hhn8329", // Exception for Max-Planck --> MPI is included in the list, skip this entry
            ]
            .indexOf(element["hasRorId"]) === -1); // remove the ROR ID, then it will not be added

        graph["@graph"] = graph["@graph"].concat(response_lrz[0]["@graph"]);

        var promises = [];

        var memberCounter = 0;
        var coappCounter = 0;
        var participantCounter = 0;
        var networkCounter = 0;

        $.each(graph["@graph"], function (index, participant) {
            if (participant.type && participant.type === "http://xmlns.com/foaf/0.1/Organization") { // Find organizations, ignore others
                let promise = new Promise((resolve, reject) => {
                    console.debug("Processing member %s | %s", index, participant["sourceSystemID"]);
                    role = onEachMember(graph, index, resolve, reject);
                    memberCounter++;

                    switch(role) {
                        case "Co-applicant":
                            coappCounter++;
                            break;
                        case "Participant":
                            participantCounter++;
                            break;
                        case "Network":
                            networkCounter++;
                            break;
                        case "Applicant":
                            break;
                        default:
                            console.error("Unknown role: ", role);
                    }
                      
                });
                promises.push(promise);
            };
        });

        var result = Promise.all(promises);
        result.then(function (data) {
            $("#loadingIndicator").hide();

            console.debug("Promises completed: %s", data.length);

            $("div.membersGrid > div").click(function () { // When card is clicked the corresponding popup is opened
                markerOpenPopup($(this)[0].id);
            });

            // Put grid in alphabetical order
            var sortList = list => [...list].sort((a, b) => {
                var A = a.textContent, B = b.textContent;
                return (A < B) ? -1 : (A > B) ? 1 : 0;
            });

            const grid = $("#membersGrid")[0]; // Get unordered grid
            const gridElements = grid.querySelectorAll("div.member");
            grid.append(...sortList(gridElements));

            $("#count").html(memberCounter);
            $("#countCoapps").html(coappCounter);
            $("#countParticipants").html(participantCounter);
            $("#countNetworks").html(networkCounter);
        });
    }).fail((errors) => {
        console.error("Error processing requests.", errors);

        $("#loadingIndicator").hide();
        $("#divMain").hide();
        $("#divFallback").show();
    });

}); // Document ready
